FROM golang:latest

RUN mkdir -p $GOPATH/src/gitlab.com/r1se/get_unique_key
ADD . /$GOPATH/src/gitlab.com/r1se/get_unique_key
WORKDIR /$GOPATH/src/gitlab.com/r1se/get_unique_key

ENV REDIS_TIME_EXPIRE=40000
ENV REDIS_URL="db:6379"
ENV REDIS_PASSWORD=""
ENV REST_PORT=":8080"
ENV GRPC_PORT=":7777"
ENV SWAGGER_FILE_PATH="./keygenerator/proto/keygenerator.swagger.json"

RUN go build
RUN go install

ENTRYPOINT /go/bin/get_unique_key

EXPOSE 8080
