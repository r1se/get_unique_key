package main

import (
	"flag"
	"gitlab.com/r1se/get_unique_key/keygenerator"
	"sync"
)

func main() {

	isDebug := flag.Bool("debug", false, "")
	flag.Parse()

	close_wg := &sync.WaitGroup{}
	close_wg.Add(1)
	keygenerator.NewKeyGeneratorServer(close_wg, *isDebug)
	close_wg.Wait()
}
