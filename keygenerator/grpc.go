package keygenerator

import (
	"github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/grpc-ecosystem/go-grpc-middleware/validator"
	"gitlab.com/r1se/get_unique_key/keygenerator/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"math"
	"net"
)

//GRPC server
func (keygen *KeyGeneratorServer) startGRPC() {
	// create a listener on TCP port
	listener, err := net.Listen("tcp", keygen.GrpcHostPort)
	if err != nil {
		log.Fatalf("Can't bind port for GRPC: %v", err)
	}

	// GRPC API
	go func() {
		opts := []grpc_recovery.Option{
			grpc_recovery.WithRecoveryHandler(keygen.RecoveryHandlerFunc),
		}

		// Create a server. Recovery handlers should typically be last in the chain so that other middleware
		// (e.g. logging) can operate on the recovered state instead of being directly affected by any panic
		grpcServer := grpc.NewServer(
			grpc_middleware.WithUnaryServerChain(
				grpc_recovery.UnaryServerInterceptor(opts...),
				grpc_validator.UnaryServerInterceptor(),
			),
			grpc.MaxRecvMsgSize(math.MaxInt32),
		)

		// attach the service to the server

		keygenerator_v1.RegisterKeyGeneratorServer(grpcServer, keygen)
		// start the server
		//TODO logger

		err := grpcServer.Serve(listener)
		if err != nil {
			log.Fatalf("Can't serve GRPC: %v", err)
		}
	}()

}

//Wrap panic auth server
func (keygen *KeyGeneratorServer) RecoveryHandlerFunc(p interface{}) (err error) {
	err = status.Errorf(codes.Internal, "%s", p)
	log.Println("GRPC error: ", err)
	return err
}
