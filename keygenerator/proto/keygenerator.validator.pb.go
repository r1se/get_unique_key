// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: keygenerator.proto

/*
Package keygenerator_v1 is a generated protocol buffer package.

It is generated from these files:
	keygenerator.proto

It has these top-level messages:
	UniqueKey
	ListKeys
	Answer
	Empty
*/
package keygenerator_v1

import go_proto_validators "github.com/mwitkow/go-proto-validators"
import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import _ "google.golang.org/genproto/googleapis/api/annotations"
import _ "github.com/mwitkow/go-proto-validators"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

func (this *UniqueKey) Validate() error {
	return nil
}
func (this *ListKeys) Validate() error {
	for _, item := range this.UniqueKeys {
		if item != nil {
			if err := go_proto_validators.CallValidatorIfExists(item); err != nil {
				return go_proto_validators.FieldError("UniqueKeys", err)
			}
		}
	}
	return nil
}
func (this *Answer) Validate() error {
	return nil
}
func (this *Empty) Validate() error {
	return nil
}
