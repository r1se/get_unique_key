package keygenerator

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/r1se/get_unique_key/keygenerator/proto"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

const (
	unlocked = iota // 0
	locked          // 1
	expired         // 2
)

//структура сервера имплементирует grpc server
type KeyGeneratorServer struct {
	redisClient                                                  *redis.Client
	redisExpieKeyTime                                            int
	redisurl, redispass, SwaggerJsonFile, RestPort, GrpcHostPort string
	close_wg                                                     *sync.WaitGroup
}

// уонструктор сервера
func NewKeyGeneratorServer(close_wg *sync.WaitGroup, debug bool) *KeyGeneratorServer {
	KeyGenerator := &KeyGeneratorServer{
		close_wg: close_wg,
	}

	var redisExpieKeyTime = os.Getenv("REDIS_TIME_EXPIRE")
	if redisExpieKeyTime == "" {
		KeyGenerator.redisExpieKeyTime = 40000 //40 second
		log.Println("OsEnv REDIS_TIME_EXPIRE don't set, use default value")
	} else {
		timems, err := strconv.Atoi(redisExpieKeyTime)
		if err != nil {
			log.Println("Can't convert REDIS_TIME_EXPIRE, use default value")
		}
		KeyGenerator.redisExpieKeyTime = timems
	}

	KeyGenerator.redisurl = os.Getenv("REDIS_URL")
	if KeyGenerator.redisurl == "" {
		log.Println("OsEnv REDIS_URL don't set, use default value")
		KeyGenerator.redisurl = "127.0.0.1:6379"
	}

	KeyGenerator.redispass = os.Getenv("REDIS_PASSWORD")

	KeyGenerator.RestPort = os.Getenv("REST_PORT")
	if KeyGenerator.RestPort == "" {
		log.Println("OsEnv REST_PORT don't set, use default value")
		KeyGenerator.RestPort = ":8080"
	}

	KeyGenerator.SwaggerJsonFile = os.Getenv("SWAGGER_FILE_PATH")
	if KeyGenerator.SwaggerJsonFile == "" {
		log.Println("OsEnv SWAGGER_FILE_PATH don't set, use default value")
		KeyGenerator.SwaggerJsonFile = "./keygenerator/proto/keygenerator.swagger.json"
	}

	KeyGenerator.GrpcHostPort = os.Getenv("GRPC_PORT")
	if KeyGenerator.GrpcHostPort == "" {
		log.Println("OsEnv GRPC_PORT don't set, use default value")
		KeyGenerator.GrpcHostPort = "0.0.0.0:6666"
	}

	KeyGenerator.init(debug)
	return KeyGenerator
}

//инициализация сервера
func (keygen *KeyGeneratorServer) init(debug bool) {

	keygen.redisClient = redis.NewClient(&redis.Options{
		Network:  "tcp",
		Addr:     keygen.redisurl,
		Password: keygen.redispass,
		DB:       0,
	})
	_, err := keygen.redisClient.Ping().Result()
	if err != nil {
		log.Fatalf("Redis cant start address: %v wtih pass: %v error: %v", keygen.redisurl, keygen.redispass, err)
	}
	//make random generator
	rand.Seed(time.Now().UnixNano())

	keygen.startGRPC()
	keygen.startREST(debug)

	// graceful shutdown
	var gracefulStop = make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)
	go func() {
		sig := <-gracefulStop
		//TODO logger
		fmt.Println("SIG is: ", sig)
		//sigContext.Warnf("Caught system sig: %+v", sig)
		keygen.Close() // освободит wg.Done() и стартовавший нас main завершится
	}()
}

//метод выдачи рандомных ключей
func (keygen *KeyGeneratorServer) randKeys(n int) (string, error) {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}

	res, err := keygen.redisClient.Get(string(b)).Int64()
	if err == redis.Nil {
		_, err := keygen.redisClient.Incr(string(b)).Result()
		if err != nil {
			log.Println("Incremetn ERROR: ", err)
			return "", err
		}
		_, err = keygen.redisClient.Expire(string(b), time.Duration(keygen.redisExpieKeyTime)*time.Millisecond).Result()
		if err != nil {
			log.Println("Expire ERROR: ", err)
			return "", err
		}
		return string(b), nil
	}
	if err != nil {
		log.Println("Get Key ERROR: ", err)
		return "", err
	}

	switch res {
	case locked:
		_, err := keygen.redisClient.Incr(string(b)).Result()
		if err != nil {
			log.Println("Incremetn ERROR: ", err)
			return "", err
		}
		return string(b), nil
	case unlocked:
		return keygen.randKeys(4)
	case expired:
		_, err := keygen.redisClient.DecrBy(string(b), 0).Result()
		if err != nil {
			log.Println("Decrement ERROR: ", err)
			return "", err
		}
		return string(b), nil
	default:
		return "", fmt.Errorf("We on default switch")
	}
}

// метод получения уникального улюча
func (keygen *KeyGeneratorServer) GetUniqueKey(context.Context, *keygenerator_v1.Empty) (*keygenerator_v1.UniqueKey, error) {
	key, err := keygen.randKeys(4)
	if err != nil {
		log.Println("Get unique key ERROR: ", err)
		return &keygenerator_v1.UniqueKey{Key: ""}, err
	}
	return &keygenerator_v1.UniqueKey{Key: key}, nil
}

// метод проверки уникального улюча
func (keygen *KeyGeneratorServer) CheckUniqueKey(ctx context.Context, key *keygenerator_v1.UniqueKey) (*keygenerator_v1.Answer, error) {

	answer := &keygenerator_v1.Answer{Answer: ""}
	if len(key.Key) != 4 {
		return answer, fmt.Errorf("Wrong size")
	}

	res, err := keygen.redisClient.Get(key.Key).Int64()
	if err == redis.Nil {
		return answer, fmt.Errorf("Key doesn't exists")
	}
	if err != nil {
		log.Println("Get ERROR: ", err)
		return answer, err
	}

	switch res {
	case locked:
		answer.Answer = "Key not used"
		return answer, nil
	case unlocked:
		_, err := keygen.redisClient.Incr(key.Key).Result()
		if err != nil {
			log.Println("Incremetn ERROR: ", err)
			return answer, err
		}
		answer.Answer = "Key is used"
		return answer, nil
	case expired:
		answer.Answer = "Key expire"
		return answer, nil
	default:
		return answer, fmt.Errorf("We on default switch")
	}
}

// метод удаления уникального ключа
func (keygen *KeyGeneratorServer) DeleteUniqueKey(ctx context.Context, key *keygenerator_v1.UniqueKey) (*keygenerator_v1.Answer, error) {

	answer := &keygenerator_v1.Answer{Answer: ""}
	if len(key.Key) != 4 {
		return answer, fmt.Errorf("Wrong size")
	}

	res, err := keygen.redisClient.Get(key.Key).Int64()
	if err == redis.Nil {
		return answer, fmt.Errorf("Key doesn't exists")
	}
	if err != nil {
		log.Println("Get ERROR: ", err)
		return answer, err
	}

	switch res {
	case locked:
		answer.Answer = "cant expire not used keys"
		return answer, nil
	case unlocked:
		_, err := keygen.redisClient.Incr(key.Key).Result()
		if err != nil {
			log.Println("Incremetn ERROR: ", err)
			return answer, err
		}
		answer.Answer = "Success to expire key: " + key.Key
		return answer, nil
	case expired:
		answer.Answer = "key already expired"
		return answer, nil
	default:
		return answer, fmt.Errorf("We on default switch")
	}
}

// метод получения массива ключей
func (keygen *KeyGeneratorServer) GetListUniqueKey(context.Context, *keygenerator_v1.Empty) (*keygenerator_v1.ListKeys, error) {

	answer := &keygenerator_v1.ListKeys{}
	var cursor uint64
	for {
		keys, cursor, err := keygen.redisClient.Scan(cursor, "", 0).Result()
		if err != nil {
			log.Println("Scan ERROR: ", err)
			fmt.Println(err)
			return nil, fmt.Errorf("Error get list of unique: %v", err)
		}
		if cursor == 0 {
			for _, value := range keys {
				res, err := keygen.redisClient.Get(value).Int64()
				if err != nil {
					log.Println("Get ERROR: ", err)
					return nil, fmt.Errorf("Error iterate list of unique: %v", err)
				}
				if res == 0 {
					answer.UniqueKeys = append(answer.UniqueKeys, &keygenerator_v1.UniqueKey{Key: value})
				}
			}
			break
		}
	}

	return answer, nil
}

//метод заершения работы сервера
func (keygen *KeyGeneratorServer) Close() {
	//TODO logger

	if keygen.redisClient != nil {
		keygen.redisClient.Close()
	}

	keygen.close_wg.Done()
}
