package keygenerator

import (
	"context"
	"fmt"
	"github.com/go-redis/redis"
	"gitlab.com/r1se/get_unique_key/keygenerator/proto"
	"sync"
	"testing"
)

func TestKeyGeneratorServer_randKeys(t *testing.T) {
	type fields struct {
		redisClient *redis.Client
		close_wg    *sync.WaitGroup
	}
	type args struct {
		n int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test generate random",
			fields{redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     "127.0.0.1:6379",
				Password: "",
				DB:       0,
			}), nil},
			args{4},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keygen := &KeyGeneratorServer{
				redisClient: tt.fields.redisClient,
				close_wg:    tt.fields.close_wg,
			}
			got, err := keygen.randKeys(tt.args.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyGeneratorServer.randKeys() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(got)
		})
	}
}

func TestKeyGeneratorServer_GetListUniqueKey(t *testing.T) {
	type fields struct {
		redisClient *redis.Client
		close_wg    *sync.WaitGroup
	}
	type args struct {
		in0 context.Context
		in1 *keygenerator_v1.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test list of keys",
			fields{redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     "127.0.0.1:6379",
				Password: "",
				DB:       0,
			}), nil},
			args{nil, nil},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keygen := &KeyGeneratorServer{
				redisClient: tt.fields.redisClient,
				close_wg:    tt.fields.close_wg,
			}
			got, err := keygen.GetListUniqueKey(tt.args.in0, tt.args.in1)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyGeneratorServer.GetListUniqueKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(got)
		})
	}
}

func TestKeyGeneratorServer_GetUniqueKey(t *testing.T) {
	type fields struct {
		redisClient *redis.Client
		close_wg    *sync.WaitGroup
	}
	type args struct {
		in0 context.Context
		in1 *keygenerator_v1.Empty
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test get keys",
			fields{redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     "127.0.0.1:6379",
				Password: "",
				DB:       0,
			}), nil},
			args{nil, nil},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keygen := &KeyGeneratorServer{
				redisClient: tt.fields.redisClient,
				close_wg:    tt.fields.close_wg,
			}
			got, err := keygen.GetUniqueKey(tt.args.in0, tt.args.in1)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyGeneratorServer.GetUniqueKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(got)
		})
	}
}

func TestKeyGeneratorServer_CheckUniqueKey(t *testing.T) {
	type fields struct {
		redisClient *redis.Client
		close_wg    *sync.WaitGroup
	}
	type args struct {
		ctx context.Context
		key *keygenerator_v1.UniqueKey
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test check keys",
			fields{redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     "127.0.0.1:6379",
				Password: "",
				DB:       0,
			}), nil},
			args{nil, &keygenerator_v1.UniqueKey{Key: "8F2q"}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keygen := &KeyGeneratorServer{
				redisClient: tt.fields.redisClient,
				close_wg:    tt.fields.close_wg,
			}
			got, err := keygen.CheckUniqueKey(tt.args.ctx, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyGeneratorServer.CheckUniqueKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(got)
		})
	}
}

func TestKeyGeneratorServer_DeleteUniqueKey(t *testing.T) {
	type fields struct {
		redisClient *redis.Client
		close_wg    *sync.WaitGroup
	}
	type args struct {
		ctx context.Context
		key *keygenerator_v1.UniqueKey
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"test delete keys",
			fields{redis.NewClient(&redis.Options{
				Network:  "tcp",
				Addr:     "127.0.0.1:6379",
				Password: "",
				DB:       0,
			}), nil},
			args{nil, &keygenerator_v1.UniqueKey{Key: "8F2q"}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			keygen := &KeyGeneratorServer{
				redisClient: tt.fields.redisClient,
				close_wg:    tt.fields.close_wg,
			}
			got, err := keygen.DeleteUniqueKey(tt.args.ctx, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("KeyGeneratorServer.DeleteUniqueKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			fmt.Println(got)
		})
	}
}
