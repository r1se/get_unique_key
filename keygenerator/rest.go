package keygenerator

import (
	"bufio"
	"context"
	"errors"
	"github.com/elazarl/go-bindata-assetfs"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/r1se/get_unique_key/keygenerator/proto"
	"gitlab.com/r1se/get_unique_key/keygenerator/proto/swagger"
	"google.golang.org/grpc"
	"io"
	"log"
	"math"
	"mime"
	"net/http"
	"net/http/pprof"
	"os"
)

//Wrap panic http server
func RecoverWrap(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		//wrap panic
		defer func() {
			r := recover()
			if r != nil {
				//find type of recover
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}
				log.Println(err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		}()
		//Serve request
		h.ServeHTTP(w, r)
	})
}

//REST API server proxy
func (keygen *KeyGeneratorServer) startREST(debug bool) {
	//make new  multiplexer for grpc-gateway
	gw := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithInsecure(), grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(math.MaxInt32))}

	//make context
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	//register mutliplexer on grpc endpoint
	err := keygenerator_v1.RegisterKeyGeneratorHandlerFromEndpoint(ctx, gw, keygen.GrpcHostPort, opts)
	if err != nil {
		log.Fatalf("Can't register handle for endpoint GRPC: %v", err)
	}

	//make new http multiplexer for add 200 answer
	mux := http.NewServeMux()
	mux.Handle("/", RecoverWrap(gw))

	// pprof for debug
	if debug {
		mux.HandleFunc("/debug/pprof/", pprof.Index)
		mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}

	keygen.ServeSwagger(mux)

	// REST API
	go func() {
		defer cancel()
		log.Println("Start listening at", keygen.RestPort)
		err := http.ListenAndServe(keygen.RestPort, RecoverWrap(mux)) // always returns a non-nil error.
		if err != nil {
			log.Fatalf("Can't start listen and serve: %v", err)
		}
	}()

}

//Server Swagger
func (keygen *KeyGeneratorServer) ServeSwagger(mux *http.ServeMux) {

	//set mime and start fileserver
	mime.AddExtensionType(".svg", "image/svg+xml")
	fileServer := http.FileServer(&assetfs.AssetFS{
		Asset:     swagger.Asset,
		AssetDir:  swagger.AssetDir,
		AssetInfo: swagger.AssetInfo,
		Prefix:    "dist",
	})

	//handle json file with swagger
	mux.HandleFunc("/swagger.json", func(w http.ResponseWriter, req *http.Request) {
		f, err := os.Open(keygen.SwaggerJsonFile)
		if err != nil {
			log.Println("Can't open file: ", err)
		} else {
			io.Copy(w, bufio.NewReader(f))
		}
	})

	//handle ui from fileserver
	mux.Handle("/swagger-ui/", http.StripPrefix("/swagger-ui/", fileServer))
}
