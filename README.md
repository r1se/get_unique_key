
Для запуска сервера в папке с docker-compose.yml файлом выполните команду:

docker-compose up

Конфиги в Dockerfile

Описание методов API:

http://localhost:8080/swagger-ui/

Зависимости для разработчиков:

go get github.com/go-redis/redis

go get github.com/elazarl/go-bindata-assetfs

go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway

go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger

go get -u github.com/golang/protobuf/protoc-gen-go

go get -u github.com/mwitkow/go-proto-validators/protoc-gen-govalidators

go get -u github.com/grpc-ecosystem/go-grpc-middleware

go get -u golang.org/x/net/context

go get -u google.golang.org/grpc